## Python3 code by Shu He from Central China Normal University
## h@mails.ccnu.edu.cn

from sys import exit

kCorr2Cum = 1  # express correlation functions in terms of cumulants
kCum2Corr = 2  # express cumulants in terms of correlation functions
kCum2Momt = 3  # express cumulants in terms of moments

class DTerm:
    RevType = {1: 2, 2: 1, 3: 3}

    def __init__(self, c=0, ops=[], opt = kCorr2Cum):
        self.opt = opt
        self.c = c
        self.ops = []
        for s in ops: # self.ops: (str, order)
            self.ops.append([s, 1])
        self.Align()

    def Align(self):
        self.ops.sort(key=lambda op: -len(op[0]))

    def AlterOp(self, i, s):
        self.ops[i][0] = ''.join(sorted(s))
        self.Align()

    def SameTerm(self, rhs):
        if len(self.ops) != len(rhs.ops):
            return False

        for i, op in enumerate(self.ops):
            if op[0] != rhs.ops[i][0]:
                return False

        return True

    def Copy(self):
        ret = DTerm()
        ret.opt = self.opt
        ret.c = self.c
        ret.ops = []
        for op in self.ops:
            ret.ops.append([op[0], op[1]])
        return ret

    def Rev(self):
        t = DTerm()
        t.c = self.c
        t.opt = DTerm.RevType[self.opt]

        t.ops.append([self.ops[0][0], 1])

        if len(self.ops) > 1:
            for op in self.ops[1:]:
                t = op[0] * t

        return t

    def __rmul__(self, rhs):
        do_next = False

        ds = rhs.split()

        if len(ds) > 1:
            dops = ds[::-1]
            s = dops[0]
            do_next = True
        else:
            s = ds[0]

        res = DTerms()

        if self.opt == kCorr2Cum:
            for i, op in enumerate(self.ops):
                t = self.Copy()

                t.c *= -op[1]
                t.ops[i][0] = ''.join(sorted(op[0] + s))
                t.ops[i][1] += 1
                t.Align()

                res.Add(t)

            t = self.Copy()
            t.ops.append([s, 1])
            t.Align()

            res.Add(t)

        elif self.opt == kCum2Corr:
            for i, op in enumerate(self.ops):
                t = self.Copy()

                t.ops[i][0] = ''.join(sorted(op[0] + s))
                t.ops[i][1] += 1
                t.Align()

                res.Add(t)

            t = self.Copy()
            t.ops.append([s, 1])
            t.Align()

            res.Add(t)

        elif self.opt == kCum2Momt:
            for i, op in enumerate(self.ops):
                t = self.Copy()

                t.ops[i][0] = ':'.join(sorted((op[0] + ':' + s).split(':')))
                t.Align()

                res.Add(t)

            Gpow = len(self.ops)

            t = self.Copy()
            t.c *= -Gpow
            t.ops.append([s, 1])
            t.Align()

            res.Add(t)

        else:
            raise RuntimeError('self.opt should be kCum2Corr, kCorr2Cum or kCum2Momt')

        if do_next:
            remain_ops = dops[1:]
            ret = remain_ops * res
            return ret
        else:
            return res

    def __str__(self):
        res = str(self.c)
        for op in self.ops:
            res += '(' + op[0] + ')'
        return res

    def __repr__(self):
        return self.__str__()

class DTerms:
    def __init__(self):
        self.ts = []

    def Add(self, t):
        new_term = True
        for it in self.ts:
            if it.SameTerm(t):
                it.c += t.c
                new_term = False

                if it.c == 0:
                    self.ts.remove(it)
                break
        if new_term:
            self.ts.append(t)

    def __rmul__(self, rhs):
        if isinstance(rhs, str):
            ds = rhs.split()
        else:
            ds = rhs
        last = self.ts

        for s in ds[::-1]:
            res = DTerms()

            for t in last:
                res += s * t

            last = res.ts

        return res

    def __iadd__(self, rhs):
        if isinstance(rhs, DTerms):
            for t in rhs.ts:
                self.Add(t)
        elif isinstance(rhs, DTerm):
            self.Add(rhs)

        return self

    def __str__(self):
        res = ','.join([str(i) for i in self.ts])
        return res

    def __repr__(self):
        return self.__str__()

if __name__ == '__main__':

    # Testing
    o1 = DTerm(1, ['a', 'a', 'a'], opt = kCorr2Cum)
    o1 = o1.Rev()
    print('c in k:', o1)
    print('------')

    for t in o1.ts:
        for i, op in enumerate(t.ops):
            t.AlterOp(i, op[0] + 'p')
    print('eff cor:', o1)

    res = DTerms()
    for i, t in enumerate(o1.ts):
        tRV = t.Rev()
        print(i, tRV)
        res += tRV

    print('k to c:', res)
    print('------')

    mnt = DTerms()
    for t in res.ts:
        ops = []
        for op in t.ops:
            ops.append(op[0])
        Kt = DTerm(t.c, ops, opt = kCum2Momt)
        Mt = Kt.Rev()
        print('C->', Kt)
        print('M->', Mt)
        mnt += Mt

    print('c in m:', mnt)
    print('%d terms' % len(mnt.ts))
    coeff = [t.c for t in mnt.ts]
    print('Coeff.:', sorted(coeff))
    print('------')
