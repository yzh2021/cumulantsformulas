#!/usr/bin/env python3

#Yu Zhang from Central China Normal University
#zhang_yu@mails.ccnu.edu.cn

import sys
import sympy
from sympy import *
import re

import formula_write
names=locals()
#===============================================
#convert track-by-track cumulant from Shu's code to raw moments
t1, t2, t3, t4, t5, t6, t7, t8 = symbols('t1 t2 t3 t4 t5 t6 t7 t8')
M = symbols('M', integer = True, postive = True)
i = Idx(symbols('i', integer = True), (0, M))
a = IndexedBase('a')
b = IndexedBase('b')
c = IndexedBase('c')
d = IndexedBase('d')
e = IndexedBase('e')
f = IndexedBase('f')

# Generation function K
K = ln(Sum(exp(t1*a[i] + t2*b[i] + t3*c[i] + t4*d[i] + t5*e[i] + t6*f[i]), (i)))

# differential to K (dK/dt)
def k_base(ord1, ord2, ord3, ord4, ord5, ord6): 
    res = diff(diff(diff(diff(diff(diff(K, t1, ord1), t2, ord2), t3, ord3), t4, ord4), t5, ord5), t6, ord6)
    return res.subs({t1: 0, t2: 0, t3: 0, t4: 0, t5: 0, t6: 0})

def func0(a=''):
    if a == '':
        return ''
    a = a.replace('/Sum(1, (i, 0, M))**16', '')
    a = a.replace('/Sum(1, (i, 0, M))**15', '')
    a = a.replace('/Sum(1, (i, 0, M))**14', '')
    a = a.replace('/Sum(1, (i, 0, M))**13', '')
    a = a.replace('/Sum(1, (i, 0, M))**12', '')
    a = a.replace('/Sum(1, (i, 0, M))**11', '')
    a = a.replace('/Sum(1, (i, 0, M))**10', '')
    a = a.replace('/Sum(1, (i, 0, M))**9', '')
    a = a.replace('/Sum(1, (i, 0, M))**8', '')
    a = a.replace('/Sum(1, (i, 0, M))**7', '')
    a = a.replace('/Sum(1, (i, 0, M))**6', '')
    a = a.replace('/Sum(1, (i, 0, M))**5', '')
    a = a.replace('/Sum(1, (i, 0, M))**4', '')
    a = a.replace('/Sum(1, (i, 0, M))**3', '')
    a = a.replace('/Sum(1, (i, 0, M))**2', '')
    a = a.replace('/Sum(1, (i, 0, M))**1', '')
    a = a.replace('/Sum(1, (i, 0, M))', '')
    a = a.replace(', (i, 0, M)', '')
    rex1=re.compile(r'Sum\([q0-9_\*]+\)')
    while 1:
        _r1=rex1.search(a)
        if _r1 == None:
            break
        mStr1=a[_r1.span()[0]:_r1.span()[1]]
        mStr1=mStr1.replace('**','_')
        mStr1=mStr1.replace('*','')
        mStr1=mStr1.replace('Sum(','j')
        mStr1=mStr1.replace(')','j')
        a = a[0:_r1.span()[0]] + mStr1 + a[_r1.span()[1]:len(a)]
    rex = r'q[0-9][0-9]_[0-9][0-9](?:_[0-9]+){0,1}'
    a='('+sympy.ccode(a)+')'
    return a


def func1(a=''):
    numList=['0','1','2','3','4','5','6','7','8','9']
    if a == '':
        return ''
    for i in range(1,20):
        b='/Sum(1, (i, 0, M))**'+str(20-i)
        a=a.replace(b, '')
    b='/Sum(1, (i, 0, M))'
    a=a.replace(b, '')
    b=', (i, 0, M)'
    a=a.replace(b, '')
    b='Sum('
    a=a.replace(b, 'v[\'')
    b=')'
    a=a.replace(b, '\']')
    b='**'
    a=a.replace(b, 'r')
    rex = r'[0-9]\*q'
    rex = re.compile(rex)
    while True:
        _ti=rex.search(a)    
        if _ti == None:
            break
        if _ti != None:
            a=a[0:_ti.span()[0]+1]+''+a[_ti.span()[1]-1:len(a)]
    #_ti = re.search(r'v\[.*\]', a)
    rex = r"v\[\'([A-Za-z0-9_]+)\'\]r"
    rex = re.compile(rex)
    while True:
        _ti=rex.search(a)
        if _ti == None:
            break
        if _ti != None:
            _eterm=a[_ti.span()[0]:_ti.span()[1]-1]
            if (_ti.span()[1]+1) == len(a): 
                a=a[0:_ti.span()[0]]+'pow('+_eterm+','+a[_ti.span()[1]:_ti.span()[1]+1]+')'+a[_ti.span()[1]+1:len(a)]
            else:
                if a[_ti.span()[1]+1] in numList:
                    a=a[0:_ti.span()[0]]+'pow('+_eterm+','+a[_ti.span()[1]:_ti.span()[1]+2]+')'+a[_ti.span()[1]+2:len(a)]
                else:
                    a=a[0:_ti.span()[0]]+'pow('+_eterm+','+a[_ti.span()[1]:_ti.span()[1]+1]+')'+a[_ti.span()[1]+1:len(a)]
    a='('+a+')'
    return a


def func2(Ord):
    #DEBUG=True
    DEBUG=False
    if Ord == 1:
        return 'jq01_01j'
    inf = formula_write.PrintOrd(Ord)
    ## 5terms
    ## example : @q01_01_02q02_03_01q01_01_02q02_03_01q02_03_01@
    pat=r'jq[0-9]+_[0-9]+r[0-9]+q[0-9]+_[0-9]+r[0-9]+q[0-9]+_[0-9]+r[0-9]+q[0-9]+_[0-9]+r[0-9]+q[0-9]+_[0-9]+r[0-9]+j'
    pat = re.compile(pat)
    while True:
        mat=pat.search(inf)
        if mat == None:
            break
        if mat != None:
            o=inf[mat.span()[0]:mat.span()[1]]
            term1=o[1:7]
            term2=o[10:16]
            term3=o[19:25]
            term4=o[28:34]
            term5=o[37:43]
            order1=int(o[8:10])
            order2=int(o[17:19])
            order3=int(o[26:28])
            order4=int(o[35:37])
            order5=int(o[44:46])
            a=str(sympy.expand(k_base(order1,order2,order3,order4,order5,0)))
            a=a.replace('a[i]', term1).replace('b[i]', term2).replace('c[i]', term3).replace('d[i]',term4).replace('e[i]',term5)
            inf=inf.replace(o, func0(a))
    if DEBUG:
        print(inf)
        print('*******************************************************')

    ## 4terms
    ## example : @q01_01_02q02_03_01q01_01_02q02_03_01@
    pat=r'jq[0-9]+_[0-9]+r[0-9]+q[0-9]+_[0-9]+r[0-9]+q[0-9]+_[0-9]+r[0-9]+q[0-9]+_[0-9]+r[0-9]+j'
    pat = re.compile(pat)
    while True:
        mat=pat.search(inf)
        if mat == None:
            break
        if mat != None:
            o=inf[mat.span()[0]:mat.span()[1]]
            term1=o[1:7]
            term2=o[10:16]
            term3=o[19:25]
            term4=o[28:34]
            order1=int(o[8:10])
            order2=int(o[17:19])
            order3=int(o[26:28])
            order4=int(o[35:37])
            a=str(sympy.expand(k_base(order1,order2,order3,order4,0,0)))
            a=a.replace('a[i]', term1).replace('b[i]', term2).replace('c[i]', term3).replace('d[i]',term4)
            inf=inf.replace(o, func0(a))
    if DEBUG:
        print(inf)
        print('*******************************************************')

    pat=r'jq[0-9]+_[0-9]+r[0-9]+q[0-9]+_[0-9]+r[0-9]+q[0-9]+_[0-9]+r[0-9]+j'
    pat = re.compile(pat)
    while True:
        mat=pat.search(inf)
        if mat == None:
            break
        if mat != None:
            o=inf[mat.span()[0]:mat.span()[1]]
            term1=o[1:7]
            term2=o[10:16]
            term3=o[19:25]
            order1=int(o[8:10])
            order2=int(o[17:19])
            order3=int(o[26:28])
            a=str(sympy.expand(k_base(order1,order2,order3,0,0,0)))
            a=a.replace('a[i]', term1).replace('b[i]', term2).replace('c[i]', term3)
            inf=inf.replace(o, func0(a))
    if DEBUG:
        print(inf)
        print('*******************************************************')

    pat=r'jq[0-9][0-9]_[0-9][0-9]r[0-9]+q[0-9][0-9]_[0-9][0-9]r[0-9]+j'
    pat = re.compile(pat)
    while True:
        mat=pat.search(inf)
        if mat == None:
            break
        if mat != None:
            o=inf[mat.span()[0]:mat.span()[1]]
            term1=o[1:7]
            term2=o[10:16]
            order1=int(o[8:10])
            order2=int(o[17:19])
            a=sympy.expand(k_base(order1,order2,0,0,0,0))
            a=str(a)
            a=a.replace('a[i]', term1).replace('b[i]', term2)
            inf=inf.replace(o, func0(a))
    if DEBUG:
        print(inf)
        print('*******************************************************')

    pat=r'jq[0-9]+_[0-9]+r[0-9]+j'
    pat = re.compile(pat)
    while True:
        mat=pat.search(inf)
        if mat == None:
            break
        if mat != None:
            o=inf[mat.span()[0]:mat.span()[1]]
            term1=o[1:7]
            order1=int(o[8:10])
            a=sympy.expand(k_base(order1,0,0,0,0,0))
            a=str(a)
            a=a.replace('a[i]', term1)
            inf=inf.replace(o, func0(a))
    return inf

#return a set of terms 
def func3(Ord):
    if Ord == 1:
        cset=set()
        cset.add('q01_01')
        return cset, 'q01_01'
    DEBUG = False
    inf = func2(Ord)
    rex = re.compile(r'j[q0-9_]+j')
    mSet=set()
    while 1:
        _mIndex=rex.search(inf)
        if _mIndex == None:
            break
        _mStr = inf[_mIndex.start():_mIndex.end()]
        _mRepacked=repacker(_mStr.replace('j',''))
        mSet.add(_mRepacked)
        inf = inf[0:_mIndex.start()]+_mRepacked+inf[_mIndex.end():len(inf)]
    return mSet, inf

def repacker(term):
    term = str(term)
    if re.search(r'\*\*', term):
        print('bad input term, power format(**) is not permitted')
        sys.exit()
    term = term.replace('*', '')
    rex = r'q[0-9][0-9]_[0-9][0-9](?:_[0-9]+){0,1}'
    mList=re.findall(rex, term)
    mVari=1
    for i in mList:
        v = i[0:6]
        if len(i) == 6:
            order = 1
        if len(i) == 8:
            order = i[7:8]
        if len(i) == 9:
            order = i[7:9]
        names[str(v)] = sympy.symbols(v)
        mVari = mVari * eval(v) ** int(order)
    mVari = str(mVari).replace('**','_').replace('*','')
    return mVari

def pow_to_mul(expr):
    """
    Convert integer powers in an expression to Muls, like a**2 => a*a.
    """
    pows = list(expr.atoms(Pow))
    if any(not e.is_Integer for b, e in (i.as_base_exp() for i in pows)):
        raise ValueError("A power contains a non-integer exponent")
    repl = zip(pows, (sympy.Mul(*[b]*e,evaluate=False) for b,e in (i.as_base_exp() for i in pows)))
    return expr.subs(repl)

if __name__ == '__main__':
    print(func3(1))
