#!/usr/bin/env python3
#using sympy as the basic tool to process
#cumulant stuff. Inspired by Shu He's code.
#Yu Zhang
#2020-7-20
#Formulas for raw moments' and central moments' convariances
#are obtained from "Advanced Theory of Statistics" by MAURICE G. KENDALL, M.A.
#Delta method is used to obtain cumulant error formula.
#Which can be refered in DOI:10.1088/0954-3899/39/2/025008 by Xiaofeng Luo.
#Relations formulas between central moments and raw moments are partly from
#DOI:10.1081/STA-120018823 by R.willink, and from wikipedia and wolfram math world.

import sympy as sym
import sympy as sympy
import re
import numpy as numpy
import math
import sys
import time

import cov

names=locals()

#==============================================================================
#central moment
u = sym.IndexedBase('u', shape=(12))
#raw moment
m = sym.IndexedBase('m', shape=(12))
#cumulant
c = sym.IndexedBase('c', shape=(12))
#factorial cumulant
fc = sym.IndexedBase('fc', shape=(12))

#number of events
n = sym.symbols('n',integer = True, positive = True)
#index number
q = sym.symbols('q',integer = True, positive = True)
#index number
r = sym.symbols('r',integer = True, positive = True)

i = sym.Idx(sym.symbols('i',integer = True), (0, n))
#==============================================================================

#==============================================================================
#convert cumulants to raw moments; efficiency uncorrected
def c2m(r):
    temp=u[1]
    for i in range(1, r):
        temp += sym.binomial(r-1, i-1)*m[r-i]*c2m(i)
    temp = m[r] - temp
    return sym.expand(temp.subs({u[1] : 0, m[0] : 1, c[1] : m[1]}))
#==============================================================================
#convert raw moments to cumulants; efficiency uncorrected
def m2c(r):
    if r == 0:
        return 1
    temp=u[1]
    for j in range(0, r):
        temp += sym.binomial(r-1, j)*m2c(j)*c[r-j]
    return sym.expand(temp.subs({u[0] : 1, u[1] : 0, m[0] : 1, m[1] : c[1]}))
#==============================================================================
#convert cumulants to central moments; efficiency uncorrected
def c2u(r):
    if r == 1:
        return m[1]
    temp=u[1]
    for i in range(1, r-1):
        temp += sym.binomial(r-1, i)*u[i]*c2u(r-i)
    temp = u[r] - temp
    return sym.expand(temp.subs({u[1] : 0, m[0] : 1, c[1] : m[1]}))
#==============================================================================
#convert central moments to cumulants; efficiency uncorrected
def u2c(r):
    if r == 0:
        return 1
    if r == 1:
        return 0
    temp=u[1]
    for j in range(0, r-1):
        temp += sym.binomial(r-1, j)*u2c(j)*c[r-j]
    return sym.expand(temp.subs({u[0] : 1, u[1] : 0, m[0] : 1, c[1] : m[1]}))
#==============================================================================
#convert central moments to raw moments; efficiency uncorrected
def u2m(r):
    temp = sym.simplify(m[0]*sym.Sum(sym.binomial(r, q)*(-1)**(r-q)*m[q]*m[1]**(r-q), (q, 0, r)))
    return temp.subs({m[0] : 1})
#==============================================================================
#convert raw moments to central moments; efficiency uncorrected
def m2u(r):
    temp = sym.simplify(m[0]*sym.Sum(sym.binomial(r, q)*u[q]*m[1]**(r-q), (q, 0, r)))
    return temp.subs({m[0] : 1, u[0] : 1, u[1] : 0})
#==============================================================================


#==============================================================================
#return covariance of central moments
#by order, r and q are central moments' order;
#efficiency uncorrected
def ucov(r,q):
    if r == 0:
        return 0
    if q == 0:
        return 0
    temp = (u[r+q] - u[r]*u[q] + r*q*u[2]*u[r-1]*u[q-1] - r*u[r-1]*u[q+1] - q*u[r+1]*u[q-1])/n
    return temp.subs({u[0] : 1, u[1] : 0})

#==============================================================================
#return covariance of raw moments
#efficiency uncorrected
def mcov(r,q):
    temp = (m[r+q] - m[r]*m[q])/n
    return temp.subs({m[0] : 1})
#==============================================================================
#retrun covariance of cumulants
#covariance is propagated from raw moments' covariance
#efficiency uncorrected
def ccov(r,q):
    Cr = c2m(r)
    Cq = c2m(q)
    temp = set(Cr.free_symbols)
    for a in Cq.free_symbols:
        temp.add(a)
    BaseIndices=[]
    for obj in temp:
        if isinstance(obj, sympy.tensor.indexed.Indexed):
            BaseIndices.append(sym.get_indices(obj)[0].pop())
    CovarianceList=[]
    for i in range(len(BaseIndices)):
        _temp=[]
        for j in range(len(BaseIndices)):
            _temp.append(mcov(BaseIndices[i], BaseIndices[j]))
        CovarianceList.append(_temp)
    CovarianceArray = numpy.array(CovarianceList)
    CrArray=numpy.array([])
    CqArray=numpy.array([])
    for i in range(len(BaseIndices)):
        CrArray = numpy.append(CrArray, sym.expand(sym.diff(Cr, m[BaseIndices[i]])))
        CqArray = numpy.append(CqArray, sym.expand(sym.diff(Cq, m[BaseIndices[i]])))
    result = sym.expand(sym.simplify(CrArray.dot(CovarianceArray).dot(CqArray.transpose()) )) 
    print("C"+str(r)+" and C"+str(q)+"'s covariance: ")
    sym.print_ccode(result)
    return result

#==============================================================================

#==============================================================================
#***efficiency uncorrected***
#obtain stat. error of Cumulant in terms of central moments
#input example:
#getErrorM(3,2) will retrive error for C3/C2
#in terms of central moments
#getErrorM(2,2) will retrive error for C1
#in terms of central moments
#cumulant order should be at least 2
#we all know C1's error is u[2]/n, cheers! This is a giant step!
#C1's error or sample mean's error is given at page 207 at 
#The Advanced theory of Statistics.
#C1's error can be obtained in function getErrorM below.
def getErrorU(r, q): 
    if r == 1 or q == 1:
        return getErrorM(r,q)
    if r == q:
        InputCum=(c2u(r))
    else:
        InputCum=(c2u(r)/c2u(q))
    temp = (InputCum).free_symbols
    BaseIndices=[]
    DiffedElement=[]
    CovarianceDict={}
    for obj in temp:
        if isinstance(obj, sympy.tensor.indexed.Indexed):
            BaseIndices.append(sym.get_indices(obj)[0].pop())
    for i in range(len(BaseIndices)):
        for j in range(len(BaseIndices)):
            CovarianceDict[(i, j)] = ucov(BaseIndices[i], BaseIndices[j])
    for obj in BaseIndices:
        DiffedElement.append(sym.expand(sym.diff(InputCum, u[obj])))
    Err = u[1]
    for i in range(len(BaseIndices)):
        for j in range(len(BaseIndices)):
            Err += sym.simplify(DiffedElement[i]*CovarianceDict[(i,j)]*n*DiffedElement[j].subs({u[1] : 0}))
    Err = Err.subs({u[1] : 0})
    Err = sym.expand(sym.simplify(Err))
    #if r == q:
    #    print('Var(C'+str(r)+'): ')
    #else:
    #    print('Var(C'+str(r)+'/C'+str(q)+'): ')
    #sym.print_ccode(Err/n)
    ##uncomment following line to print latex format
    #print(sym.latex(Err)+"/n")
    return Err
#==============================================================================
#***efficiency uncorrected***
#obtain variance of central moment as a function of raw moments.
#this function is same with ucov(r,q) which return variance as a function
#of central moments
def getErrorUbM(r): 
    InputCum=(u2m(r))
    temp = (InputCum).free_symbols
    BaseIndices=[]
    DiffedElement=[]
    CovarianceDict={}
    for obj in temp:
        if isinstance(obj, sympy.tensor.indexed.Indexed):
            BaseIndices.append(sym.get_indices(obj)[0].pop())
    for i in range(len(BaseIndices)):
        for j in range(len(BaseIndices)):
            CovarianceDict[(i, j)] = mcov(BaseIndices[i], BaseIndices[j])
    for obj in BaseIndices:
        DiffedElement.append(sym.expand(sym.diff(InputCum, m[obj])))
    Err = u[1]
    for i in range(len(BaseIndices)):
        for j in range(len(BaseIndices)):
            Err += sym.simplify(DiffedElement[i]*CovarianceDict[(i,j)]*n*DiffedElement[j].subs({u[1] : 0}))
    sym.simplify(Err.subs({u[1] : 0}))
    #Err /= n
    print('Var(u'+str(r)+'): ', sym.expand(Err.subs({u[1] : 0}))/n)
#==============================================================================
#***efficiency uncorrected***
#obtain error formulas of Cumulant in terms of raw moments
#input example:
#getErrorM(3,2) will retrive error for C3/C2
#in terms of raw moments
#getErrorM(1,1) will retrive error for C1
#in terms of raw moments
def getErrorM(r, q): 
    #DEBUG=True
    DEBUG=False
    if r == q:
        InputCum=(c2m(r))
    else:
        InputCum=(c2m(r)/c2m(q))
    temp = (InputCum).free_symbols
    BaseIndices=[]
    DiffedElement=[]
    CovarianceDict={}
    for obj in temp:
        if isinstance(obj, sympy.tensor.indexed.Indexed):
            BaseIndices.append(sym.get_indices(obj)[0].pop())
    for i in range(len(BaseIndices)):
        for j in range(len(BaseIndices)):
            CovarianceDict[(i, j)] = mcov(BaseIndices[i], BaseIndices[j])
    for obj in BaseIndices:
        DiffedElement.append(sym.expand(sym.diff(InputCum, m[obj])))
    if DEBUG:
        print("Input: ",InputCum)
        print("Diffed: ",DiffedElement)
        print("BaseIndices: ",BaseIndices)
        print("Covariance: ",CovarianceDict)
    Err = u[1]
    for i in range(len(BaseIndices)):
        for j in range(len(BaseIndices)):
            Err += sym.simplify(DiffedElement[i]*CovarianceDict[(i,j)]*n*DiffedElement[j])
    Err = Err.subs({u[1] : 0})
    Err = sym.expand(sym.simplify(Err))
    #if r == q:
    #    print('Var(C'+str(r)+'): ')
    #else:
    #    print('Var(C'+str(r)+'/C'+str(q)+'): ')
    #sym.print_ccode(sym.simplify(sym.expand(Err)))
    #uncomment following line to print latex format
    #print(sym.latex(sym.expand(Err))+"/n")
    return Err
#==============================================================================
#***efficiency uncorrected***
##convert raw moments expression to central moments expression
def ConvertR2U(expr):
    expr = str(expr)
    expr = expr.replace("m","m2u")
    expr = expr.replace("[","(")
    expr = expr.replace("]",")")
    expr = eval(expr)
    expr = sym.expand(sym.simplify(expr))
    #sym.print_ccode(expr)
    #print(sym.latex(expr))
    return expr
#==============================================================================
#***efficiency uncorrected***
##convert central moments expression to raw moments expression
def ConvertU2R(expr):
    expr = str(expr)
    expr = expr.replace("u","u2m")
    expr = expr.replace("[","(")
    expr = expr.replace("]",")")
    expr = eval(expr)
    expr = sym.expand(sym.simplify(expr))
    #sym.print_ccode(expr)
    #print(sym.latex(expr))
    return expr
#==============================================================================
#***efficiency uncorrected***
##convert raw moments expression to cumulant expression
def ConvertR2C(expr):
    expr = str(expr)
    expr = expr.replace("m","m2c")
    expr = expr.replace("[","(")
    expr = expr.replace("]",")")
    expr = eval(expr)
    expr.subs({m[1] : c[1]})
    expr = sym.expand(sym.simplify(expr))
    #sym.print_ccode(expr)
    #print(sym.latex(expr)
    return expr
#==============================================================================
#***efficiency uncorrected***
##convert cumulant expression to raw moments expression
def ConvertC2R(expr):
    expr = str(expr)
    expr = expr.replace("c","c2m")
    expr = expr.replace("[","(")
    expr = expr.replace("]",")")
    expr = eval(expr)
    expr = sym.expand(sym.simplify(expr))
    #sym.print_ccode(expr)
    #print(sym.latex(expr))
    return expr
#==============================================================================
#***efficiency uncorrected***
##convert central moments expression to cumulant expression
def ConvertU2C(expr):
    expr = str(expr)
    expr = expr.replace("u","u2c")
    expr = expr.replace("[","(")
    expr = expr.replace("]",")")
    expr = eval(expr)
    expr.subs({m[1] : c[1]})
    expr = sym.expand(sym.simplify(expr))
    #sym.print_ccode(expr)
    #print(sym.latex(expr))
    return expr
#==============================================================================
#***efficiency uncorrected***
##convert cumulant expression to central moments expression
def ConvertC2U(expr):
    expr = str(expr)
    expr = expr.replace("c","c2u")
    expr = expr.replace("[","(")
    expr = expr.replace("]",")")
    expr = eval(expr)
    expr.subs({m[1] : c[1]})
    expr = sym.expand(sym.simplify(expr))
    return expr
    #sym.print_ccode(expr)
    #print(sym.latex(expr))
#==============================================================================
#***efficiency uncorrected***
#convert factorial cumulant(correlation function) to cumulant
N = sym.symbols('N',integer = True, positive = True)
def fc2c(ord):
    terms=N
    for i in range(1, ord):
        terms *= N-i
    terms = sym.expand(terms)
    terms = str(terms)
    for i in range(2, 12):
        terms = terms.replace("N**"+str(i),"c["+str(i)+"]")
    terms = terms.replace("N","c[1]")
    terms = eval(terms)
    #print(terms)
    return terms
#===============================================================
#***efficiency uncorrected***
#return the error of factorial cumulant
def getErrorFC(r, q): 
    #DEBUG=True
    DEBUG=False
    if r == q:
        InputCum=(ConvertC2R(fc2c(r)))
    else:
        InputCum=(ConvertC2R(fc2c(r)/fc2c(q)))
    temp = (InputCum).free_symbols
    BaseIndices=[]
    DiffedElement=[]
    CovarianceDict={}
    for obj in temp:
        if isinstance(obj, sympy.tensor.indexed.Indexed):
            BaseIndices.append(sym.get_indices(obj)[0].pop())
    for i in range(len(BaseIndices)):
        for j in range(len(BaseIndices)):
            CovarianceDict[(i, j)] = mcov(BaseIndices[i], BaseIndices[j])
    for obj in BaseIndices:
        DiffedElement.append(sym.expand(sym.diff(InputCum, m[obj])))
    if DEBUG:
        print("Input: ",InputCum)
        print("Diffed: ",DiffedElement)
        print("BaseIndices: ",BaseIndices)
        print("Covariance: ",CovarianceDict)
    Err = u[1]
    for i in range(len(BaseIndices)):
        for j in range(len(BaseIndices)):
            Err += sym.simplify(DiffedElement[i]*CovarianceDict[(i,j)]*n*DiffedElement[j])
    Err = Err.subs({u[1] : 0})
    Err = sym.expand(sym.simplify(Err))
    if r == q:
        print('Var(FC'+str(r)+'): ')
    else:
        print('Var(FC'+str(r)+'/FC'+str(q)+'): ')
    #sym.print_ccode(sym.simplify(sym.expand(Err)))
    #print(sym.latex(sym.expand(Err))+"/n")
    return Err
#===============================================================
#Test C2/C1's delta function in terms of raw moments 
#with manually delta deduction, and with error propagation(covariance involved)
#Conclusion : They are identical.
if 0:
    g1=sym.diff(((m[2]-m[1]**2)/m[1]),m[1])
    g2=sym.diff(((m[2]-m[1]**2)/m[1]),m[2])
    v11=mcov(1,1)
    v12=mcov(1,2)
    v21=mcov(2,1)
    v22=mcov(2,2)
    print(":")
    print("manually delta deduction:")
    print("Var(C2/C1): ",sym.expand(g1*g1*v11 + g1*g2*v12 + g2*g1*v21+g2*g2*v22) )
    print("delta function: ")
    getErrorM(2,1)

    print("error propagation with covariance: ")
    c21=m[2]/m[1] - m[1]
    var1=m[2]-m[1]**2
    var2=m[4]-m[2]**2
    cov=m[3]-m[1]*m[2]
    g1=sym.diff(c21, m[1])
    g2=sym.diff(c21, m[2])
    print("Var(C2/C1): ",sym.expand( g1*g1*var1/n + 2*g1*g2*cov/n + g2*g2*var2/n  ))
#===============================================================
#***efficiency uncorrected***
#this part print efficiency uncorrected statistical uncertainty formulas
# for cumulant c1 to c6.
if 0:
    output=open("Variance_C1-6.txt","w")
    print("Variance(C1):")
    print(sympy.ccode((ConvertR2C(getErrorM(1,1)))/n))
    output.write("Variance(C1):")
    output.write(sympy.ccode((ConvertR2C(getErrorM(1,1)))/n))
    output.write("\n")
    print("\n")
    print("Variance(C2):")
    print(sympy.ccode((ConvertU2C(getErrorU(2,2)))/n))
    output.write("Variance(C2):")
    output.write(sympy.ccode((ConvertU2C(getErrorU(2,2)))/n))
    output.write("\n")
    print("\n")
    print("Variance(C3):")                               
    print(sympy.ccode((ConvertU2C(getErrorU(3,3)))/n))
    output.write("Variance(C3):")                               
    output.write(sympy.ccode((ConvertU2C(getErrorU(3,3)))/n))
    output.write("\n")
    print("\n")
    print("Variance(C4):")                               
    print(sympy.ccode((ConvertU2C(getErrorU(4,4)))/n))
    output.write("Variance(C4):")                               
    output.write(sympy.ccode((ConvertU2C(getErrorU(4,4)))/n))
    output.write("\n")
    print("\n")
    print("Variance(C5):")                               
    print(sympy.ccode((ConvertU2C(getErrorU(5,5)))/n))
    output.write("Variance(C5):")                               
    output.write(sympy.ccode((ConvertU2C(getErrorU(5,5)))/n))
    output.write("\n")
    print("\n")
    print("Variance(C6):")                               
    print(sympy.ccode((ConvertU2C(getErrorU(6,6)))/n))
    output.write("Variance(C6):")                               
    output.write(sympy.ccode((ConvertU2C(getErrorU(6,6)))/n))
    output.write("\n")
    print("\n")
    print("Variance(C2/C1):")                               
    print(sympy.ccode((ConvertR2C(getErrorM(2,1))/n)))
    output.write("Variance(C2/C1):")                               
    output.write(sympy.ccode((ConvertR2C(getErrorM(2,1))/n)))
    output.write("\n")
    print("\n")
    print("Variance(C3/C2):")                               
    print(sympy.ccode((ConvertU2C(getErrorU(3,2))/n)))
    output.write("Variance(C3/C2):")                               
    output.write(sympy.ccode((ConvertU2C(getErrorU(3,2))/n)))
    output.write("\n")
    print("\n")
    print("Variance(C4/C2):")                               
    print(sympy.ccode((ConvertU2C(getErrorU(4,2))/n)))
    output.write("Variance(C4/C2):")                               
    output.write(sympy.ccode((ConvertU2C(getErrorU(4,2))/n)))
    output.write("\n")
    print("\n")
    print("Variance(C5/C1):")                               
    print(sympy.ccode((ConvertR2C(getErrorM(5,1))/n)))
    output.write("Variance(C5/C1):")                               
    output.write(sympy.ccode((ConvertR2C(getErrorM(5,1))/n)))
    output.write("\n")
    print("\n")
    print("Variance(C6/C2):")                               
    print(sympy.ccode((ConvertU2C(getErrorU(6,2))/n)))
    output.write("Variance(C6/C2):")                               
    output.write(sympy.ccode((ConvertU2C(getErrorU(6,2))/n)))
    output.write("\n")
    output.close()
#=====================================================
#accept form1/2 as string
def GetCov(x, y):
    return  sympy.symbols(cov.repacker(sympy.Mul(x, y, evaluate=False)))- x * y
#=====================================================
#***efficiency corrected***
def GetCumulant(Ord):
    s, v= cov.func3(Ord)
    return s, v
#=====================================================
#***efficiency corrected***
#return statistical error of track-by-track cumulant/ratio formulas
#input interger value for i and j
#
GetCumulant(3)

def GetError(i, j):
    si, vi = cov.func3(i)
    sj, vj = cov.func3(j)
    tSet = set.union(si, sj)
    tStr = list(tSet)
    tObj = [sympy.symbols(v) for v in tStr]
    for iObj in tObj:
        names[str(iObj)] = iObj
    CovarianceList=[]
    #taking covariance will create new symbols
    #but not for dot product
    if 1:
        for _i in range(len(tObj)):
            _tmpList=[]
            for _j in range(len(tObj)):
                _tmpTerm = GetCov(tObj[_i], tObj[_j])
                _tmpList.append(_tmpTerm)
            CovarianceList.append(_tmpList)
    CovarianceArray = numpy.array(CovarianceList)
    _C=''
    if i == j:
        _C = eval(vi)
    else:
        _C = eval(vi) / eval(vj)
    CArray=numpy.array([])
    for iObj in tObj:
        _cterm = sympy.diff(_C, iObj)
        CArray = numpy.append(CArray, _cterm)
    _product1 = CArray.dot(CovarianceArray).dot(CArray.transpose())
    rSet = _product1.free_symbols
    _product1 = sympy.ccode(_product1)
    return rSet, _product1
#=====================================================
#mix moments




#=====================================================
'''
print(fc2c(2))
print(fc2c(3))
print(fc2c(4))
print(fc2c(5))
print(fc2c(6))

-c[1] + c[2]
2*c[1] - 3*c[2] + c[3]
-6*c[1] + 11*c[2] - 6*c[3] + c[4]
24*c[1] - 50*c[2] + 35*c[3] - 10*c[4] + c[5]
-120*c[1] + 274*c[2] - 225*c[3] + 85*c[4] - 15*c[5] + c[6]
'''
#efficiency corrected
#get factorial cumulant in terms of moments
def GetFactorialCumulant(Ord):
    _cterm1, _vt1 = cov.func3(1)
    _cterm2, _vt2 = cov.func3(2)
    _cterm3, _vt3 = cov.func3(3)
    _cterm4, _vt4 = cov.func3(4)
    _cterm5, _vt5 = cov.func3(5)
    _cterm6, _vt6 = cov.func3(6)
    _cterm1 = list(_cterm1)
    _cterm2 = list(_cterm2)
    _cterm3 = list(_cterm3)
    _cterm4 = list(_cterm4)
    _cterm5 = list(_cterm5)
    _cterm6 = list(_cterm6)
    _cterm1 = [sympy.symbols(v) for v in _cterm1]
    _cterm2 = [sympy.symbols(v) for v in _cterm2]
    _cterm3 = [sympy.symbols(v) for v in _cterm3]
    _cterm4 = [sympy.symbols(v) for v in _cterm4]
    _cterm5 = [sympy.symbols(v) for v in _cterm5]
    _cterm6 = [sympy.symbols(v) for v in _cterm6]
    for iTerm in _cterm1:
        names[str(iTerm)] = iTerm
    for iTerm in _cterm2:
        names[str(iTerm)] = iTerm
    for iTerm in _cterm3:
        names[str(iTerm)] = iTerm
    for iTerm in _cterm4:
        names[str(iTerm)] = iTerm
    for iTerm in _cterm5:
        names[str(iTerm)] = iTerm
    for iTerm in _cterm6:
        names[str(iTerm)] = iTerm
    C1 = eval(_vt1)
    C2 = eval(_vt2)
    C3 = eval(_vt3)
    C4 = eval(_vt4)
    C5 = eval(_vt5)
    C6 = eval(_vt6)
    FC1 = (C1)
    FC2 = (-C1 + C2)
    FC3 = (2*C1 - 3*C2 + C3)
    FC4 = (-6*C1 + 11*C2 - 6*C3 + C4)
    FC5 = (24*C1 - 50*C2 + 35*C3 - 10*C4 + C5)
    FC6 = (-120*C1 + 274*C2 - 225*C3 + 85*C4 - 15*C5 + C6)
    Terms = ''
    if Ord == 1:
        Terms = FC1
    if Ord == 2:
        Terms = FC2
    if Ord == 3:
        Terms = FC3
    if Ord == 4:
        Terms = FC4
    if Ord == 5:
        Terms = FC5
    if Ord == 6:
        Terms = FC6
    rSet = Terms.free_symbols
    #rSet = set.union(set(_cterm1),set(_cterm2),set(_cterm3),set(_cterm4),set(_cterm5),set(_cterm6))
    _product1 = sympy.ccode(Terms)
    return rSet, _product1
#=====================================================
#efficiency corrected
#get error formula of factorial cumulant
#program runs really slow for case like GetFCError(4,3), GetFCError(5,4)
#see lines below, only defined input Ord1 and Ord2 are accepted 
def GetFCError(Ord1, Ord2):
    if Ord1 > 6 or Ord2 > 6:
        print("within 6th order!")
        return 
    _cterm1, _vt1 = cov.func3(1)
    _cterm2, _vt2 = cov.func3(2)
    _cterm3, _vt3 = cov.func3(3)
    _cterm4, _vt4 = cov.func3(4)
    _cterm5, _vt5 = cov.func3(5)
    _cterm6, _vt6 = cov.func3(6)
    _cterm1 = list(_cterm1)
    _cterm2 = list(_cterm2)
    _cterm3 = list(_cterm3)
    _cterm4 = list(_cterm4)
    _cterm5 = list(_cterm5)
    _cterm6 = list(_cterm6)
    _cterm1 = [sympy.symbols(v) for v in _cterm1]
    _cterm2 = [sympy.symbols(v) for v in _cterm2]
    _cterm3 = [sympy.symbols(v) for v in _cterm3]
    _cterm4 = [sympy.symbols(v) for v in _cterm4]
    _cterm5 = [sympy.symbols(v) for v in _cterm5]
    _cterm6 = [sympy.symbols(v) for v in _cterm6]
    for iTerm in _cterm1:
        names[str(iTerm)] = iTerm
    for iTerm in _cterm2:
        names[str(iTerm)] = iTerm
    for iTerm in _cterm3:
        names[str(iTerm)] = iTerm
    for iTerm in _cterm4:
        names[str(iTerm)] = iTerm
    for iTerm in _cterm5:
        names[str(iTerm)] = iTerm
    for iTerm in _cterm6:
        names[str(iTerm)] = iTerm
    C1 = eval(_vt1)
    C2 = eval(_vt2)
    C3 = eval(_vt3)
    C4 = eval(_vt4)
    C5 = eval(_vt5)
    C6 = eval(_vt6)
    FC1 = C1
    FC2 = -C1 + C2
    FC3 = 2*C1 - 3*C2 + C3
    FC4 = -6*C1 + 11*C2 - 6*C3 + C4
    FC5 = 24*C1 - 50*C2 + 35*C3 - 10*C4 + C5
    FC6 = -120*C1 + 274*C2 - 225*C3 + 85*C4 - 15*C5 + C6
    Terms = ''
    if Ord1 == 2 and Ord2 == 2:
        Terms = FC2
    if Ord1 == 3 and Ord2 == 3:
        Terms = FC3
    if Ord1 == 4 and Ord2 == 4:
        Terms = FC4
    if Ord1 == 5 and Ord2 == 5:
        Terms = FC5
    if Ord1 == 6 and Ord2 == 6:
        Terms = FC6
    if Ord1 == 2 and Ord2 == 1:
        Terms = FC2 / FC1
    if Ord1 == 3 and Ord2 == 1:
        Terms = FC3 / FC1
    if Ord1 == 4 and Ord2 == 1:
        Terms = FC4 / FC1
    if Ord1 == 5 and Ord2 == 1:
        Terms = FC5 / FC1
    if Ord1 == 6 and Ord2 == 1:
        Terms = FC6 / FC1
    if Ord1 == 3 and Ord2 == 2:
        Terms = FC3 / FC2
    if Ord1 == 4 and Ord2 == 3:
        Terms = FC4 / FC3
    if Ord1 == 5 and Ord2 == 4:
        Terms = FC5 / FC4
    if Ord1 == 6 and Ord2 == 5:
        Terms = FC6 / FC5
    mSet = set(Terms.free_symbols)
    mList = list(mSet)
    for iTerm in mList:
        names[str(iTerm)] = iTerm
    CovarianceList=[]
    for _iTerm in range(len(mList)):
        _CList=[]
        for _jTerm in range(len(mList)):
            _CTerm = GetCov(mList[_iTerm], mList[_jTerm])
            _CList.append(_CTerm)
        CovarianceList.append(_CList)
    CovarianceArray = numpy.array(CovarianceList)
    TermArray=numpy.array([])
    for iTerm in mList:
        _clterm = sympy.diff(Terms, iTerm)
        TermArray = numpy.append(TermArray, _clterm)
    _product1 = TermArray.dot(CovarianceArray).dot(TermArray.transpose())
    rSet = _product1.free_symbols
    _product1 = sympy.ccode(_product1)
    return rSet, _product1
#=====================================================
