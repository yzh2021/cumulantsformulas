## Python3 code by Shu He from Central China Normal University
## h@mails.ccnu.edu.cn

from CumCorr import *
import sympy

CumOrd = 2

# C1 ~ CN (N = CumOrd)

def PrintOrd(cOrd):
    if cOrd < 2:
        raise runtime_except('i should >= 2')

    o = DTerm(1, ['a'] * cOrd, opt = kCorr2Cum)
    o = o.Rev()

    for t in o.ts:
        for i, op in enumerate(t.ops):
            t.AlterOp(i, op[0] + 'p')

    res = DTerms()
    for i, t in enumerate(o.ts):
        tRV = t.Rev()
        res += tRV

    allRes = ''
    maxLen = 0
    for i, t in enumerate(res.ts):
        #print '>', t
        comb = {}
        for op in t.ops:
            if op[0] in comb.keys():
                comb[op[0]][0] += 1
            else:
                comb[op[0]] = [1, op[0].count('a'), op[0].count('p')]

        nspar = ''
        fstr = True
        for k in comb:
            if not fstr:
                nspar += 'q'
            else:
                fstr = False
            v = comb[k]
            nspar += '%02d_%02dr%02d' % (v[1], v[2], v[0])
        vres = 'jq%sj' % (nspar)
        tcof = t.c
        cof = ''
        if tcof > 0:
            cof = '+%d' % (tcof)
        else:
            cof = '%d' % (tcof)
        allRes += '%s*%s' % (cof, vres)

        cLen = len(comb.keys())
        if cLen > maxLen:
            maxLen = cLen

    #print '# C%d max len %d' % (cOrd, maxLen)
    #print 'k to c:', res
    #print allRes
    #print '#------'

    return allRes
if __name__ == '__main__':
    print(PrintOrd(4))
